using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;

[assembly: AssemblyCompany("Mind Compression")]
[assembly: AssemblyProduct("MCFM 05")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyTitle("MCFM 05")]
[assembly: AssemblyDescription("MindCompression FM 2005 Memory Editor")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("..\\..\\mckey.snk")]
[assembly: AssemblyCopyright("Copyright (c) 2004 MindCompression")]
[assembly: ComVisible(false)]
[assembly: PermissionSet(SecurityAction.RequestMinimum, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\r\n               version=\"1\">\r\n   <IPermission class=\"System.Security.Permissions.StrongNameIdentityPermission, mscorlib, Version=1.0.5000.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\r\n                version=\"1\"\r\n                PublicKeyBlob=\"0024000004800000940000000602000000240000525341310004000001000100679B86F6D559C13DA977D6CAD55E41DE1025F44DE70538EE3446177E7652248EACC0FFF42D253693AFBBDA7594A463B1883FE80AE1622D26EA0344B8E7F1DAE25C9F7F6F31820A12880AE36CF54E2343E06BD73C3FC9B2327FCDBB1421A105208DB4810A4C997040EA9EA8BF1AC20198F3DDE707CCB268202EE3E4C815AF07EE\"/>\r\n</PermissionSet>\r\n")]
[assembly: AssemblyVersion("0.5.0.0")]
