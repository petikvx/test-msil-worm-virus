using MindCompression.Memory;

namespace MindCompression.DataObjects;

public class CoachMentalSkills : Skills
{
	public CoachMentalSkills(int skillsAddress, ProcessMemory pm)
		: base(skillsAddress, pm)
	{
	}
}
