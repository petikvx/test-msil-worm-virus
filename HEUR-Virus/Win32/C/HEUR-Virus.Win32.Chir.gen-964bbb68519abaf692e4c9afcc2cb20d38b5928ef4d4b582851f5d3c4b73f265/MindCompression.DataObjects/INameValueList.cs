using System.Collections;

namespace MindCompression.DataObjects;

public interface INameValueList
{
	ArrayList List { get; }
}
