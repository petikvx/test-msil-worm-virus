using System;

namespace MindCompression.DataObjects;

public sealed class SkillOffsets : CalculatedOffsets
{
	private static Type _type = typeof(SkillOffsets);

	public static int Crossing = CalculatedOffsets.Calculate(_type, 49);

	public static int Dribbling = CalculatedOffsets.Calculate(_type, 50);

	public static int Finishing = CalculatedOffsets.Calculate(_type, 51);

	public static int Heading = CalculatedOffsets.Calculate(_type, 52);

	public static int LongShots = CalculatedOffsets.Calculate(_type, 53);

	public static int Marking = CalculatedOffsets.Calculate(_type, 54);

	public static int OffTheBall = CalculatedOffsets.Calculate(_type, 55);

	public static int Passing = CalculatedOffsets.Calculate(_type, 56);

	public static int PenaltyTaking = CalculatedOffsets.Calculate(_type, 57);

	public static int Tackling = CalculatedOffsets.Calculate(_type, 58);

	public static int Creativity = CalculatedOffsets.Calculate(_type, 59);

	public static int Handling = CalculatedOffsets.Calculate(_type, 60);

	public static int AerialAbility = CalculatedOffsets.Calculate(_type, 61);

	public static int CommandOfArea = CalculatedOffsets.Calculate(_type, 62);

	public static int Communication = CalculatedOffsets.Calculate(_type, 63);

	public static int Kicking = CalculatedOffsets.Calculate(_type, 64);

	public static int Throwing = CalculatedOffsets.Calculate(_type, 65);

	public static int Anticipation = CalculatedOffsets.Calculate(_type, 66);

	public static int Decisions = CalculatedOffsets.Calculate(_type, 67);

	public static int OneOnOnes = CalculatedOffsets.Calculate(_type, 68);

	public static int Positioning = CalculatedOffsets.Calculate(_type, 69);

	public static int Reflexes = CalculatedOffsets.Calculate(_type, 70);

	public static int FirstTouch = CalculatedOffsets.Calculate(_type, 71);

	public static int Technique = CalculatedOffsets.Calculate(_type, 72);

	public static int LeftFoot = CalculatedOffsets.Calculate(_type, 73);

	public static int RightFoot = CalculatedOffsets.Calculate(_type, 74);

	public static int Flair = CalculatedOffsets.Calculate(_type, 75);

	public static int Corners = CalculatedOffsets.Calculate(_type, 76);

	public static int Teamwork = CalculatedOffsets.Calculate(_type, 77);

	public static int Workrate = CalculatedOffsets.Calculate(_type, 78);

	public static int Longthrows = CalculatedOffsets.Calculate(_type, 79);

	public static int Eccentricity = CalculatedOffsets.Calculate(_type, 80);

	public static int RushingOut = CalculatedOffsets.Calculate(_type, 81);

	public static int TendencyToPunch = CalculatedOffsets.Calculate(_type, 82);

	public static int Acceleration = CalculatedOffsets.Calculate(_type, 83);

	public static int Freekicks = CalculatedOffsets.Calculate(_type, 84);

	public static int Strength = CalculatedOffsets.Calculate(_type, 85);

	public static int Stamina = CalculatedOffsets.Calculate(_type, 86);

	public static int Pace = CalculatedOffsets.Calculate(_type, 87);

	public static int Jumping = CalculatedOffsets.Calculate(_type, 88);

	public static int Influence = CalculatedOffsets.Calculate(_type, 89);

	public static int Dirtyness = CalculatedOffsets.Calculate(_type, 90);

	public static int Balance = CalculatedOffsets.Calculate(_type, 91);

	public static int Bravery = CalculatedOffsets.Calculate(_type, 92);

	public static int Consistency = CalculatedOffsets.Calculate(_type, 93);

	public static int Aggression = CalculatedOffsets.Calculate(_type, 94);

	public static int Agility = CalculatedOffsets.Calculate(_type, 95);

	public static int ImportantMatches = CalculatedOffsets.Calculate(_type, 96);

	public static int InjuryProness = CalculatedOffsets.Calculate(_type, 97);

	public static int Versatility = CalculatedOffsets.Calculate(_type, 98);

	public static int NaturalFitness = CalculatedOffsets.Calculate(_type, 99);

	public static int Determination = CalculatedOffsets.Calculate(_type, 100);

	public static int Composure = CalculatedOffsets.Calculate(_type, 101);

	public static int Concentration = CalculatedOffsets.Calculate(_type, 102);
}
