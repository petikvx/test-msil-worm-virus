namespace MindCompression.Utils;

public enum VersionInfo
{
	v500DemoStrawberry,
	v500DemoVanilla,
	v500Retail,
	v501Retail,
	v502Retail,
	v503Retail,
	v505Retail
}
