using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyFileVersion("3.0.26.1")]
[assembly: Guid("c9c74d8c-a080-41c1-8521-4148c8764789")]
[assembly: ComVisible(false)]
[assembly: AssemblyTitle("hpCaslNotification")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCompany("Hewlett-Packard Development Company L.P.")]
[assembly: AssemblyCopyright("Copyright © 2008-2009 Hewlett-Packard Development Company L.P.")]
[assembly: AssemblyProduct("hpCaslNotification")]
[assembly: AssemblyVersion("1.0.1.0")]
