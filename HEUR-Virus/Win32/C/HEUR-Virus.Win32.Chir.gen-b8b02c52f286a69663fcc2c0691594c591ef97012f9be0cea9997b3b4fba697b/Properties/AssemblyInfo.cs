using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyTrademark("")]
[assembly: Guid("826dc582-52fa-44c8-b8b3-5bc06d037926")]
[assembly: AssemblyCopyright("Copyright © Hewlett-Packard Company 2008")]
[assembly: AssemblyProduct("WinDefrag")]
[assembly: AssemblyCompany("Hewlett-Packard Company")]
[assembly: AssemblyConfiguration("")]
[assembly: ComVisible(false)]
[assembly: AssemblyDescription("")]
[assembly: AssemblyTitle("WinDefrag")]
[assembly: AssemblyVersion("1.0.0.0")]
