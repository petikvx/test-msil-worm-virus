using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Hijack This")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCopyright("© 2007 Trend Micro Inc")]
[assembly: ComVisible(false)]
[assembly: AssemblyFileVersion("2.0.0.0002")]
[assembly: AssemblyDescription("Hijack This")]
[assembly: AssemblyProduct("Hijack This")]
[assembly: AssemblyCompany("Trend Micro Inc")]
[assembly: Guid("15229162-03f1-422b-8a8f-302417af9c0c")]
[assembly: AssemblyVersion("2.0.0.2")]
