using System.ComponentModel.Design;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using bin.My;

namespace ns0;

[HideModuleName]
[CompilerGenerated]
[DebuggerNonUserCode]
[StandardModule]
internal sealed class Class6
{
	[HelpKeyword("My.Settings")]
	internal static MySettings Settings => MySettings.Default;
}
