using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using Microsoft.VisualBasic.ApplicationServices;

namespace Cursor.My;

[GeneratedCode("MyTemplate", "8.0.0.0")]
[EditorBrowsable(EditorBrowsableState.Never)]
internal class MyApplication : ConsoleApplicationBase
{
	[DebuggerNonUserCode]
	public MyApplication()
	{
		throw new ApplicationException();
	}
}
